#!/bin/bash

LOCALHOST=output/localhost.xml
EXTERNAL=output/external.xml
TMPFILE=/tmp/portscan_local
TMPFILE2=/tmp/portscan_remote


diff(){
  awk 'BEGIN{RS=ORS=" "}
       {NR==FNR?a[$0]++:a[$0]--}
       END{for(k in a)if(a[k])print k}' <(echo -n "${!1}") <(echo -n "${!2}")
}

#Scan on LO
echo "Scanning Local Loopback"
nmap -T4 -A localhost -oG $LOCALHOST 1> /dev/null

#Scan External Interface
IP="$(hostname -I | cut -f1 -d ' ')" # grep IP Address
echo "Scanning $IP"
nmap -T4 -A $IP -oG $EXTERNAL 1> /dev/null

echo "Done Scanning"

#Parse the report to usable format
./scanreport.sh -f $LOCALHOST | grep tcp | cut -f1 > $TMPFILE

#Parse the external report to usable format
./scanreport.sh -f $EXTERNAL | grep tcp | cut -f1 > $TMPFILE2

#Place into an array
IFS=$'\n' read -d '' -r -a lines_local  < $TMPFILE

#Place into an array
IFS=$'\n' read -d '' -r -a lines_ext  < $TMPFILE

#Print test 
echo "Line 0: ${lines_local[0]}"
echo "Line 0: ${lines_ext[0]}"

Array3=($(diff ${lines_local[@]} ${lines_local[@]}))
echo ${Array3[@]}


