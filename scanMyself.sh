#Scan Myself on lo
echo "Scanning Local loopback"
nmap -T4 -A localhost  -oG output/localhost.xml 1> /dev/null
echo "Done"

#Scan Myself externally
IP="$(hostname  -I | cut -f1 -d' ')" # grab ip address
echo "Scanning $IP"
nmap -T4 -A $IP -oG output/external.xml 1> /dev/null
echo "Done"
